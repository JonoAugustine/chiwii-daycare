import * as firebase from "firebase/app";

import "firebase/auth";
import "firebase/firestore";

import * as dao from "./dao";

const config = {
  apiKey: "AIzaSyBJn2HQWY9L0NGUFz5MZMSjYnOC6aOIDB0",
  authDomain: "chiwii-daycare.firebaseapp.com",
  databaseURL: "https://chiwii-daycare.firebaseio.com",
  projectId: "chiwii-daycare",
  storageBucket: "chiwii-daycare.appspot.com",
  messagingSenderId: "16825675939",
  appId: "1:16825675939:web:71a03ebfae5f9e862e8f16",
  measurementId: "G-431KZK4W67"
};

const app = firebase.initializeApp(config);

const db = app.firestore();

const auth = app.auth();

export { db, auth, dao };
