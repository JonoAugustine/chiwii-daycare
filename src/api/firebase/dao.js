import { db, auth } from ".";

export const users = db.collection("users");

export const createUser = async (email, password, ign) => {
  // Create Auth
  const cred = await auth.createUserWithEmailAndPassword(email, password);
  // Set display name
  await cred.user.updateProfile({ displayName: ign });
  // Create db instance
  await users
    .doc(cred.user.uid)
    .set({ uid: cred.user.uid, ign: ign, email: email });
};


