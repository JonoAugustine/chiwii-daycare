import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/dashboard",
    name: "dashboard",
    component: () => import("@/views/Dashboard")
  },
  {
    path: "/account",
    name: "account",
    // Lazy load
    component: () => import("@/views/Account.vue")
  }
];

const router = new VueRouter({ routes });

export default router;
