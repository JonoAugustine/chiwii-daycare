const { setup, teardown } = require("./helper");
const { mock } = require("./mock");

describe("No-Auth User rules", () => {
  let db;
  let users;

  beforeEach(async () => {
    db = await setup();
    users = db.collection("users");
  });

  afterEach(async () => await teardown());

  // Read

  test("Cannot list", async () => {
    await expect(users.get()).toDeny();
  });

  test("Cannot get", async () => {
    await expect(users.doc("_").get()).toDeny();
  });

  // Create

  test("Cannot add", async () => {
    await expect(users.doc(mock.user().uid).set(mock.user())).toDeny();
  });

  // Mutate

  test("Cannot update", async () => {
    db = await setup(null, mock.collection.users());
    users = db.collection("users");

    await expect(users.doc(mock.user().uid).update({})).toDeny();
  });

  test("Cannot delete", async () => {
    db = await setup(null, mock.collection.users());
    users = db.collection("users");

    await expect(users.doc(mock.user().uid).delete()).toDeny();
  });
});

describe("Auth No-Admin User Read/Create rules", () => {
  let db;
  let users;

  beforeEach(async () => {
    db = await setup(mock.auth());
    users = db.collection("users");
  });

  afterEach(async () => await teardown());

  // Read

  test("Cannot list", async () => await expect(users.get()).toDeny());

  test("Cannot get", async () => {
    await expect(users.doc(mock.user().uid).get()).toDeny();
  });

  // Create

  test("Cannot add at wrong id", async () => {
    await expect(users.doc("_").set(mock.user())).toDeny();
  });

  test("Cannot add with wrong id", async () => {
    await expect(users.doc(mock.user().uid).set(mock.user("_"))).toDeny();
  });

  test("Cannot add with wrong id", async () => {
    await expect(
      users.doc(mock.user().uid).set(mock.user(false, false, "_@_.com"))
    ).toDeny();
  });

  test("Cannot add extra fields", async () => {
    await expect(
      users.doc(mock.user().uid).set({ ...mock.user(), f: null })
    ).toDeny();
  });

  test("Cannot add null fields", async () => {
    await expect(
      users.doc(mock.user().uid).set({ uid: null, ign: null, email: null })
    ).toDeny();
  });

  test("Cannot add long ign", async () => {
    await expect(
      users.doc(mock.user().uid).set(mock.user(false, "_".repeat(121)))
    ).toDeny();
  });

  test("Can add valid", async () => {
    await expect(users.doc(mock.user().uid).set(mock.user())).toAllow();
  });
});

describe("Auth No-Admin User Mutate rules", () => {
  let db;
  let users;

  beforeEach(async () => {
    db = await setup(mock.auth(), mock.collection.users());
    users = db.collection("users");
  });

  afterEach(async () => await teardown());

  // Update

  test("Cannot update other", async () => {
    await expect(users.doc(mock.userOther().uid).update({})).toDeny();
  });

  test("Cannot change uid without new auth", async () => {
    await expect(users.doc(mock.user().uid).update({ uid: "_" })).toDeny();
  });

  test("Cannot change email without new auth", async () => {
    await expect(
      users.doc(mock.user().uid).update({ email: "_@_._" })
    ).toDeny();
  });

  test("Cannot change name without new auth", async () => {
    await expect(users.doc(mock.user().uid).update({ ign: "_" })).toDeny();
  });

  test("Can update with new auth", async () => {
    db = await setup(
      { ...mock.auth(), name: "_", email: "_" },
      mock.collection.users()
    );
    users = db.collection("users");

    await expect(
      users
        .doc(mock.user().uid)
        .update({ ...mock.user(), ign: "_", email: "_" })
    ).toAllow();
  });

  // Delete

  test("Cannot delete other", async () => {
    await expect(users.doc(mock.userOther().uid).delete()).toDeny();
  });

  test("Can delete self", async () => {
    await expect(users.doc(mock.user().uid).delete()).toAllow();
  });
});

describe("Auth Trainer User Rules", () => {
  let db;
  let users;

  beforeEach(async () => {
    db = await setup(mock.auth(false, true), mock.collection.users());
    users = db.collection("users");
  });

  afterEach(async () => await teardown());

  test("Cannot delete other", async () => {
    await expect(users.doc(mock.userOther().uid).delete()).toDeny();
  });

  test("Can read", async () => await expect(users.get()).toAllow());
});

describe("Auth Admin User Rules", () => {
  let db;
  let users;

  beforeEach(async () => {
    db = await setup(mock.auth(true), mock.collection.users());
    users = db.collection("users");
  });

  afterEach(async () => await teardown());

  test("Can delete other", async () => {
    await expect(users.doc(mock.userOther().uid).delete()).toAllow();
  });

  test("Can read", async () => await expect(users.get()).toAllow());
});
