const { setup, teardown } = require("./helper");
const { mock, nullify } = require("./mock");

describe("No-Auth Request Rules", () => {
  let db;
  let requests;

  beforeEach(async () => {
    db = await setup();
    requests = db.collection("requests");
  });

  afterEach(async () => await teardown());

  test("Cannot list", async () => await expect(requests.get()).toDeny());
  test("Cannot get", async () =>
    await expect(requests.doc("_ev").get()).toDeny());

  test("Cannot create", async () => {
    await expect(requests.add(mock.request.ev())).toDeny();
  });

  test("Cannot update", async () => {
    await expect(requests.doc("_ev").update({})).toDeny();
  });

  test("Cannot delete", async () => {
    await expect(requests.doc("_ev").delete()).toDeny();
  });
});

describe("Auth Request rules", () => {
  let db;
  let requests;

  beforeEach(async () => {
    db = await setup(mock.auth(), mock.collection.requests());
    requests = db.collection("requests");
  });

  afterEach(async () => teardown());

  // Read
  test("Cannot list", async () => {
    await expect(requests.get()).toDeny();
  });

  test("Cannot get other", async () => {
    await expect(requests.doc("_ev_other").get()).toDeny();
  });

  test("Can get own", async () => {
    await expect(requests.doc("_ev").get()).toAllow();
  });

  // Create

  test("Cannot add empty", async () => {
    await expect(requests.add({})).toDeny();
  });

  test("Cannot add null fields (ev)", async () => {
    await expect(requests.add(nullify(mock.request.ev()))).toDeny();
  });

  test("Cannot add null fields (level)", async () => {
    await expect(requests.add(nullify(mock.request.level()))).toDeny();
  });

  test("Cannot add other uid", async () => {
    await expect(requests.add(mock.request.ev(mock.authOther()))).toDeny();
  });

  test("Cannot add other ign", async () => {
    await expect(
      requests.add(mock.request.ev({ ...mock.auth(), ign: "_" }))
    ).toDeny();
  });

  // TODO more tests
});
