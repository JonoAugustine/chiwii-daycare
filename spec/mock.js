const auth = (admin, trainer) => ({
  uid: "userID",
  name: "userName",
  email: "email@email.com",
  email_verified: true,
  admin: admin == true,
  trainer: trainer == true || admin == true
});

const authOther = (admin, trainer) => ({
  ...auth(admin, trainer),
  uid: "userTwoID",
  name: "userTwoName"
});

const user = (uid, ign, email) => ({
  uid: uid ? uid : auth().uid,
  ign: ign ? ign : auth().name,
  email: email ? email : auth().email
});

const userOther = () => ({
  uid: authOther().uid,
  ign: authOther().name,
  email: authOther().email
});
const request = {
  ev: _auth => ({
    uid: _auth ? _auth.uid : auth().uid,
    ign: _auth ? _auth.name : auth().name,
    state: "w",
    details: {
      pokemon: {
        name: "poke name",
        id: "unique pokemon ID"
      },
      spread: {
        atk: 0,
        def: 0,
        spd: 0,
        spatk: 0,
        spdef: 0,
        hp: 0
      }
    }
  }),
  level: _auth => ({
    uid: _auth ? _auth.uid : auth().uid,
    ign: _auth ? _auth.name : auth().name,
    state: "w",
    details: {
      pokemon: {
        name: "poke name",
        id: "unique pokemon ID"
      },
      start: 1,
      end: 100
    }
  })
};
const collection = {
  users: () => {
    let m = {};
    m[`users/${user().uid}`] = user();
    m[`users/${userOther().uid}`] = userOther();
    return m;
  },
  requests: () => {
    let m = {};
    m[`requests/_ev`] = request.ev();
    m[`requests/_level`] = request.level();
    m[`requests/_ev_other`] = request.ev(authOther());
    m[`requests/_level_other`] = request.level(authOther());
    return m;
  }
};

module.exports.mock = {
  auth: auth,
  authOther: authOther,
  user: user,
  userOther: userOther,
  request: request,
  collection: collection
};

const _nullify = obj => {
  const nulled = {};
  for (const k in obj) {
    nulled[k] =
      typeof obj[k] === "object"
        ? (nulled[k] = _nullify(obj[k]))
        : (nulled[k] = null);
  }
  return nulled;
};

module.exports.nullify = _nullify;
